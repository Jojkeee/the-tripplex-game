// TripleXGame.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>
#include <ctime>

void DoSomeSettings()
{
    HANDLE hWnd = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD bufferSize = { 400, 200 };
    SetConsoleScreenBufferSize(hWnd, bufferSize);
    setlocale(LC_ALL, "russian_russia");
    srand(time(NULL));
}

void PrintLogo()
{
    std::cout << "\n\n\n"
        "                   #@(*                                                        \n"
        "                  @@   @@                                       .%*            \n"
        "                 &@    &@@                                    /@@&@&           \n"
        "                 @&     ,@@                                  &@@ @@            \n"
        "                @@        @@                                .@@@@@             \n"
        "              ,@@@.        @@            %%&&@&@%           @@@,               \n"
        "             @@@@@(        *@@    *@@@@@*%#&#@@@@,       (@@%@                 \n"
        "           .@@@@@@@@@       @@*  @@    @%&&, @@&% &@@@@&@@/#@&                 \n"
        "          .@@@@@@@@@@@@@@@@@@@(@@@@    .#&&@@@.       @@@*   &@@/              \n"
        "          @@@@@@@@@@@@@@                          /@@@@@@@.     @/             \n"
        "         @@@@@@@@@@@@@@@@#                      @@@@@@@@@@@@    @.             \n"
        "        (@@@@@@@@@@@@@@@@@@                  @@@@@@@@@@@@@@@@   ,              \n"
        "        @@@@@@@@@@@@@@@@@@@@&             @@@@@@@@@@@@@@@@@@@@ .               \n"
        "        *@@@@@@@@@@@@@@@@@@@@@         %@@@@@@@@@@@@@@@@@@@@@@*(/              \n"
        "         %@@@@@@@@@@@@@@@@@@@@@@& ,@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@             \n"
        "           (@@@@@@@@@@@@@@@@@@@@&@/@@@@@@@@@@@@@@@@@@@@@@@@@@@@   &*           \n"
        "               %@@@@@@@@@@/        @@@@@@@@@@@@@@@@@@@@@@@@@@@@/@@@            \n"
        "                                   (@@@@@@@@@@@@@@@@@@@@@@@@@@                 \n"
        "                                      .@@@@@@@@@@@@@@@@@@@@@@                  \n"
        "                                           *@@@@@@@@@@@@,                      \n\n";

                                                                                                            
}

void PrintIntroduction(int Difficulty)
{
    std::cout << "\n�� ������ � ������ � ����� �������. �� �������. �� �������� � �����. � ��� ��� ������� " << Difficulty;
                 "\n��� ������ ������ �� ������. Ÿ ������ �� ������.\n� ��� ������ ���� ������. ������, ����� ����� ����������� � ������.\n"
                 "���� ���������� �� �������� ���� ���������� ��������� �����... \n"
                 "���� ����� ������ ���������� ���� � �����, ����� ����������...\n\n";
}

bool PlayGame(int Difficulty)
{
    PrintIntroduction(Difficulty);
    
    int CodeA = rand() % Difficulty + Difficulty;
    int CodeB = rand() % Difficulty + Difficulty;
    int CodeC = rand() % Difficulty + Difficulty;

    const int CodeSum = CodeA + CodeB + CodeC;
    const int CodeProd = CodeA * CodeB * CodeC;


    //Prtint CodeSum and CodeProd
    std::cout << "+ �� ������ ����� ��� ����� � ����";
    std::cout << "\n+ ��� ������������ � �����: " << CodeSum;
    std::cout << "\n+ ������������ ���� ���� �����: " << CodeProd << std::endl << std::endl;

    // Store player guess
    int GuessA, GuessB, GuessC;
    std::cin >> GuessA >> GuessB >> GuessC;

    std::cout << "�� �����, ��� ��� ��� " << GuessA << GuessB << GuessC << std::endl;

    const int GuessSum = GuessA + GuessB + GuessC;
    const int GuessProd = GuessA * GuessB * GuessC;


    // Check if the players guess is correct
    if (CodeSum == GuessSum && CodeProd == GuessProd)
    {
        std::cout << "\n*** ��� �������! �� ���������� � ������! ***\n";
        return true;
    }
    else
    {
        std::cout << "\n*** �� ��������� ������ ��������� ���������� �����, �� ������ �� �������! ***\n";
        return false;
    }
}

int main()
{
    DoSomeSettings();

    int LevelDifficulty = 1;
    int const MaxDifficulty = 5;

    while (LevelDifficulty <= MaxDifficulty) // Loop the game untill all levels completed
    {
        bool bLevelComplete = PlayGame(LevelDifficulty);
        std::cin.clear();  //clear any errors
        std::cin.ignore(); // Discard the buffer

        if (bLevelComplete)
        {
            ++LevelDifficulty;

            std::cout << "\n\n������ ���� ������ ������ ������� ���� ����, ��� �� �������, ��� ������ � ���� ��� ����������...\n\n";
        }
    }

    std::cout << "����������, �� �������-�� �������� ������ ������� ������ � �������!";

    return 0;
}

